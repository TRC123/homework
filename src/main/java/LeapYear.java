public class LeapYear {
    /**
     * Display the number of days in month February from a year between 1900-2016 that is read from argument
     *
     */
    public static void main(String[] args) {
        int year = Integer.parseInt(args[0]);
        boolean isLeapYear;
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    isLeapYear = true;
                }
                else {
                    isLeapYear = false;
                }
            }
            else {
                isLeapYear = true;
            }
        }
        else {
            isLeapYear = false;
        }
        if (isLeapYear == true) {
            System.out.println("Feb are 29 de zile");
        }
        else {
            System.out.println("Feb are 28 de zile");
        }
    }









}



