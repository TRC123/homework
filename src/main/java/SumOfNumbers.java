public class SumOfNumbers {
    /**
     * Calculate the sum of the first 100 numbers higher than 0. Name the class (and java file) SumOfNumbers.
     * @author Catalin Troaca
     */

    public static void main(String[] args) {
        int y = 0;
        for (int x = 1; x <= 100; x++) {
            System.out.println(y += x);
        }
    }
}
