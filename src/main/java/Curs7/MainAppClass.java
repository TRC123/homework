package Curs7;

public class MainAppClass {

    public static void main(String[] args) throws Exception {
//        throwMyException();
//        System.out.println("Un mesaj");
        try {
            System.out.println(circleArea(-5));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

//        System.out.println(circleArea(-5));

        try {
            System.out.println(circleArea(-5));
        } catch (IllegalArgumentException | ArithmeticException e) { //multiple exceptions
            System.out.println(e.getMessage());
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Index out of bound");
        } catch (Exception e) {
            System.out.println("Other exceptions");
        }
        finally {
            System.out.println("This block will always run");
        }

        try {
            newThrowException();
        }
        catch (MyCustomException e) {
            System.out.println(e.getMessage());
        }

    }

    private static void throwMyException() {
        throw new IllegalArgumentException("Aici am aruncat exceptia pentru ca ...");
    }

    private static double circleArea(double radius) throws Exception {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be a positive value, your value is " + radius);
        }
        return Math.PI * radius * radius;
    }

    private static void newThrowException () throws MyCustomException {
        throw new MyCustomException("This is my custom exception!", -200);
    }
}
