package Curs6_Homework;

public class Room {

    private RoomType _type;

    public RoomType get_type() {
        return _type;
    }

    @Override
    public String toString(){
        return get_type().toString();
    }
}
