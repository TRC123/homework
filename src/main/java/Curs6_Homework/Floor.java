package Curs6_Homework;

import java.util.HashMap;
import java.util.Map;

public class Floor {
    HashMap<Room,Integer> rooms = new HashMap<Room,Integer>();

    public void setRooms(HashMap<Room, Integer> rooms) {
        this.rooms = rooms;
    }

    Integer floorNumber;
    public Floor (Integer floorNumber) {
        this.floorNumber = floorNumber;
    }

    @Override
    public String toString () {
        String description = "Floor number " + floorNumber + " contains the following rooms: " + "\r\n";
        for (Map.Entry<Room,Integer> camera: rooms.entrySet()
        ) {
            description += camera.getKey().toString() + " : " + camera.getValue() + "\r\n";
        }
        return description;
    }


}