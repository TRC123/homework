package Curs6_Homework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainClass {
    public static void main(String[] args) {

        HashMap<Room, Integer> rooms1 = new HashMap<Room, Integer>();
        rooms1.put(new Office(), 1);
        rooms1.put(new Toilet(), 2);
        rooms1.put(new Kitchen(), 1);
        rooms1.put(new ConferenceRoom(), 3);
        Floor floor1 = new Floor(1);
        floor1.setRooms(rooms1);

        HashMap<Room, Integer> rooms2 = new HashMap<Room, Integer>();
        rooms2.put(new Office(), 2);
        rooms2.put(new Toilet(), 2);
        rooms2.put(new Kitchen(), 1);
        rooms2.put(new ConferenceRoom(), 4);
        Floor floor2 = new Floor(2);
        floor2.setRooms(rooms2);

        HashMap<Room, Integer> rooms3 = new HashMap<Room, Integer>();
        rooms3.put(new Toilet(), 2);
        rooms3.put(new ConferenceRoom(), 6);
        Floor floor3 = new Floor(3);
        floor3.setRooms(rooms3);

        List<Floor> floorList = new ArrayList<Floor>();
        floorList.add(floor1);
        floorList.add(floor2);
        floorList.add(floor3);

        Building bld1 = new Building();
        bld1.setFloorList(floorList);
        System.out.println(bld1.toString());
    }

}

