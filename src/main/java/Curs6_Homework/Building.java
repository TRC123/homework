package Curs6_Homework;


import java.util.*;

/**
 * Building structure:
 * • 1 Building
 * • 3 Floors
 * • 1st floor: 1 office space , 2 toilets, 1 kitchen, 3 conference rooms
 * • 2nd floor: 2 office spaces , 2 toilets, 1 kitchen, 4 conference rooms
 * • 3rd floor: 2 toilets, 6 conference rooms
 */

public class Building {
    List<Floor> floorList = new ArrayList<Floor>();

    @Override
    public String toString() {
        String description = "These are the floors available for the building: "+ "\r\n";
        for (Floor floor : floorList ) {
        description += floor.toString() + "\r\n";
        };
        return description;
    }

    public void setFloorList(List<Floor> floorList) {
        this.floorList = floorList;
    }
}