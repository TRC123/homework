package Curs6;

import homestuff.Triangle;
import javafx.collections.transformation.SortedList;
import jdk.internal.org.objectweb.asm.tree.analysis.Analyzer;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Collections {
    public static void showArrays() {
        int[] arrays = {1, 3, 4, 5};
        System.out.println("Element pozitia 3 : " + arrays[2]);

        Car[] Cars = {new Truck(), new Truck()};

        //display entire array
        for (int i = 0; i <= arrays.length - 1; i++) {
            System.out.println(arrays[i]);
        }

        //display with forEach
        for (int el : arrays) {
            System.out.println(el);
        }

        String[] names = {"Ana", "Mihail", "Catalin"};
        for (String nume : names) {
            System.out.println(nume);
        }
    }

    //  int [] numbers = new int[100];  <- ineficient

    public static void displayList(List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void showList() {
        List arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add("Ana");
        arrayList.add("Ana are mere");
        arrayList.add(1, 2);
        displayList(arrayList);

        arrayList.remove("Ana");
        displayList(arrayList);

        List<String> names = new ArrayList<String>();
        names.add("Ion");
        names.add("Vasile");
        names.add("Vasile");
        names.add("Ana");
        System.out.println("Size of names: " + names.size() + " -> " + names.toString());
        System.out.println(names.get(1));

        java.util.Collections.sort(names);
        System.out.println(names);

    }

    //set - list with unique elements
    public static void showSets() {
        Set<Integer> setList = new HashSet<Integer>();
        setList.add(1);
        setList.add(2);
        setList.add(3);
        setList.add(3);

        System.out.println(setList.toString());

    }

    public static void displayHashMap(Map hashMap){
        for (Object key : hashMap.keySet()){
            //key -> values
            System.out.println(key + " -> " + hashMap.get(key));
        }
    }

    public static void showHashMap(){
        Map<String,Integer> hashMap = new HashMap<String,Integer>();
        hashMap.put("Laptop",3);
        hashMap.put("PC",5);
        hashMap.put("PC",2);
        hashMap.put("Mobile",20);

        displayHashMap(hashMap);

    }

    public static void main(String[] args) {
        //showArrays();
        //showList();
        showSets();
//        showHashMap();
    }


}
