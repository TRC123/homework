package Curs6;

public class MainTest {

    public static void main(String[] args) {
        Truck car = new Truck();
        car.horn();
        AbstractCar car2 = new Bus();
        car2.stop();

        //USE ENUM
        System.out.println(Browsers.CHROME);
        System.out.println(Browsers.CHROME.name());

        if (Browsers.CHROME.name().equals("CHROME")) {
            System.out.println("CHROME browser is used");
        }
    }
}
