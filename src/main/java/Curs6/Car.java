package Curs6;

public interface Car {

    public void startEngine();

    public void stopEngine();

    public void accelerate(Integer Delta);

    public void brake();

    public void shiftUp();
}
