public class PrimeNumbers {
    /**
     * Display all the prime numbers lower than 1 000 000. Name the class (and java file) PrimeNumbers
     *
     * @author Catalin Troaca
     */
    public static void main(String[] args) {
        int PRIME_TESTED = 1000000;
        for ( int numberToBeTested = 2; numberToBeTested <= PRIME_TESTED; numberToBeTested++) {

            boolean isPrime = true;
            for (int x = 2; x <= numberToBeTested / 2; x++) {
                if (numberToBeTested % x == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.println(numberToBeTested);
            }
        }
    }
}
