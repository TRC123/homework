package Curs4;

public class Library {

    public static void main(String[] args) {

        Author Cucuveanu = new Author("Ionel Cucuveanu", "cucuveanu@gmail.com");
        Book Cregulescu = new Book("Viata Este Grea", 2020, Cucuveanu.getName(), 30);

        Author Brebulescu = new Author("Cristian Brebulescu", "brebulescucristian@yahoo.com");
        Book bijnita = new Book("Treci incoa nu mai sta", 2021, Brebulescu.getName(), 12312);

        System.out.println("Book " + Cregulescu.getName() + " ( " + Cregulescu.getPrice() + " RON ) "+ ", by " + Cucuveanu.getName() + ", published in " + Cregulescu.getYear() + ".");
        System.out.println();
        System.out.println("Book " + bijnita.getName() + " ( " + bijnita.getPrice() + " RON ) "+ ", by " + Brebulescu.getName() + ", published in " + bijnita.getYear() + ".");

    }

}
