package Curs4;

public class Book {
    public String name;
    public int year;
    public String author;
    public double price;

    public Book(String name, int year, String author, double price) {
            this.name = name;
            this.year = year;
            this.author = author;
            this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getYear() {
        return year;
    }

}
