package Curs4;

public class Draw {

    private static void drawShapeCorners(int width, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if (j == 0 || j == (height - 1)) {
                    if (i == 0 || i == (width - 1)) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public static void drawShapeOutline(int width, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if (j == 0 || j == (height - 1)) {
                    System.out.print("*");
                }
                else {
                    if (i == 0 || i == (width - 1)) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static void drawFullShape(int width, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < (width - 1); i++) {
                System.out.print("*");
            }
            System.out.println(" ");
        }
    }
    public static void main(String[] args) {
        drawFullShape(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeOutline(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }

}


