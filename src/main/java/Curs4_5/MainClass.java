package Curs4_5;

import homestuff.Shape;
import homestuff.Triangle;

import java.awt.*;

public class MainClass {

    public static void main(String[] args) {
        Car logan = new Car(Color.GREEN, (byte) 5, 150);
        logan.start();
        System.out.println(logan.toString());
        logan.start();
        logan.start();
        System.out.println(logan.toString());


        //-------------------------------------


        Triangle tr = new Triangle(Color.BLACK);
        tr.draw();
        tr.erase();

        System.out.println("Verifiy polymorfism...");
        Shape shapeTr = new Triangle(Color.GREEN);
        Shape shape = new Shape(Color.RED);

        shapeTr.draw();
        shapeTr.erase();
        shape.draw();
        shape.erase();


        System.out.println("-----------------Inheritance Exercise--------");
        Person person1= new Person("Ion Vasile", 13, "1231223123123123", "234234234","male");
        Teacher teacher1 = new Teacher("Popescu Vasilica", 45, "123112233123123123", "12312312", "F", "Math", 3456);
        Student student1 = new Student("Gianluca Vamaiotu", 18, "sharomeria 23", "123123123", "ce vrea el");
        System.out.println(person1.toString());
        System.out.println(teacher1.toString());
        System.out.println(student1.toString());

    }



}
