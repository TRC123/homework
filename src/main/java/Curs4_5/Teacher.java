package Curs4_5;

public class Teacher extends Person {
    private String discipline;
    private int prodID;


    public Teacher(String name, int age, String address, String cnp, String gender) {
        super(name, age, address, cnp, gender);
    }
    public Teacher(String name, int age, String address, String cnp, String gender, String discipline, int prodID) {
        super(name, age, address, cnp, gender);
        this.discipline = discipline;
        this.prodID = prodID;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public int getProdID() {
        return prodID;
    }

    public void setProdID(int prodID) {
        this.prodID = prodID;
    }

    @Override
    public String toString() {
        return super.toString() +
                "discipline='" + discipline + '\'' +
                ", prodID=" + prodID +
                '}';
    }
}
