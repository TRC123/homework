package homestuff;

public class exercitiicurs4 {

    static boolean isDivizor (int a, int d) {
        return a % d == 0;
    }

    static boolean isEven (int a){
        return isDivizor(a,2); // same as return a % 2 == 0;

        //return a % 2 == 0;
        //        if (a % 2 == 0) {
        //            return true;
        //        }
    }

    static boolean isPrime(int a) {
        for (int i = 2; i < a/2; i++){
            if (isDivizor(a,i)) {
                return false;
            }
        }
        return  true;
    }

    static float computeProduct(float a, float b) {
        return a * b;
    }

    static void computeSum(int a, int b) {
        System.out.println("a = " + a + " b = " + b);
        System.out.println(a + b);
    }

    public static void main(String[] args) {
        computeSum(12, 15);
        computeSum(11 , 14);

        float result=computeProduct(12, 41f);
        //System.out.println(result);
        System.out.println(computeProduct(12, 41f));

        System.out.println(isEven(12));

        System.out.println(isPrime(67));

        Person p1 = new Person();
        Person p2 = new Person();

        Circle circle1 = new Circle();
        circle1.printCircle();
        circle1.setRadius(2f);
        circle1.printCircle();
        System.out.println(circle1.radius);
        System.out.println(circle1.getArea());

        Circle c2 = new Circle();
        c2.setRadius(9f);
        System.out.println(c2.getArea());

    }
}
