package homestuff;

import com.sun.java.swing.plaf.windows.WindowsTextAreaUI;

public class draw {

    public static void main(String[] args) {
        drawFullShape (Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeOutline (Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));

    }

    private static void drawShapeCorners(int width, int height) {
        for (int y = 0; y < height; y++) {
            for (int i = 0; i < width; i++) {
                if (y == 0 || y == height - 1) {
                    System.out.println("*");
                }
            }
        }
    }
        private static void drawShapeOutline(int width, int height) {
        for (int y = 0 ; y < height; y ++) {
            for (int i = 0; i < width; i++) {
                if (y == 0 || y == height - 1) {
                    System.out.println("*");
                } else {
                    if (i == 0 || i == width - 1) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
        }
            System.out.println();
    }

    private static void drawFullShape(int width, int height) {
        for (int y = 0 ; y < height; y ++) {
            for (int i = 0 ; i < width ; i++){
                System.out.print("*");
            }
            System.out.println();
        }
    }

}
