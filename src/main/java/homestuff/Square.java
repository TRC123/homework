package homestuff;

import java.awt.*;

public class Square extends Shape {

    private double squareSide;

    public Square() {
        super(Color.BLACK);
    }

    public Square(double squareSide, Color color) {
        super(color);
        this.squareSide = squareSide;
    }

    public Square(double squareSide) {
        super(Color.BLACK);
        this.squareSide = squareSide;
    }

    protected void setSide(double s) {
        squareSide = s;

    }

    public double getArea() {
        //return squareSide * squareSide;
        return Math.pow(squareSide, 2);
    }

    void printSquare() {
        System.out.println("Avem un patrat cu latura " + squareSide + " si cu aria de " + getArea());
    }

}
