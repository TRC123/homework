
import Curs7.Calculator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class CalculatorTest {

    static Calculator c;

    @BeforeClass
    public static void beforeTest() {
        c = new Calculator();
    }

    @Test
    public void testSum01() {
//        System.out.println(c.compute(2,7,"+"));
        Assert.assertEquals(9, c.compute(2, 7, "+"), 0);
    }

    @Test
    public void testSum02() {
        Assert.assertEquals(124535535, c.compute(123123123, 1412412, "+"), 0);
    }

    @Test
    public void testSum03() {
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test
    public void testDiff01() {
        Assert.assertEquals(900, c.compute(1000, 100, "-"), 0);
    }

    @Test
    public void testProduct01() {
        Assert.assertEquals(81, c.compute(9, 9, "*"), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnsupported01() {
        Assert.assertEquals(2, c.compute(10, 5, "^"), 0);
    }

    @Test
    public void testDiv01() {
        Assert.assertEquals(2, c.compute(10, 5, "/"), 0);
    }

    @Test
    public void testDiv02() {
        Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
    }

    @Test
    public void testSqrt01() {
        Assert.assertEquals(1.4142, c.compute(2, 0, "SQRT"), 0.001);
    }

    // This is the correct way to test if an exception is expected to be raised
    @Test(expected = IllegalArgumentException.class) //metoda corecta a testarii pentru tratarea acesto evenimente
    public void testDiv03() {
        Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
    }

    // This is just to catch the exception and the test will be passed either way. Please do not use this example !!
//    @Test()
//    public void testDiv04() { //nu este recomandat sa folosim try/catch in aceste situatii
//        try {
//            Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
//        } catch (IllegalArgumentException e) {
//            System.out.println(e.getMessage());
//        }
//    }

    // TEMA - de adaugat alte asserturi pt fiecare operatiune , putem incerc si jassert sau altele

    /**
     * This is where the homework begins !!!
     * I started using AssertJ for these exercises
     */
    @Test
    public void homeworkExSum01() {
        assertThat(c.compute(10, 3, "+")).isEqualTo(13);
    }

    @Test
    public void homeworkExSum02() {
        assertThat(c.compute(10, 3, "+")).isNotEqualTo(50);
    }

    @Test
    public void homeworkExDif01() {
        assertThat(c.compute(75, 5, "-")).isEqualTo(70);
    }

    @Test
    public void homeworkExDif02() {
        assertThat(c.compute(75, 5, "-")).isNotEqualTo(80);
    }

    @Test
    public void homeworkExProd01() {
        assertThat(c.compute(3, 3, "*")).isEqualTo(9);
    }

    @Test
    public void homeworkExProd02() {
        assertThat(c.compute(15, 5, "*")).isNotEqualTo(8);
    }

    @Test
    public void homeworkExDiv01() {
        assertThat(c.compute(100, 5, "/")).isEqualTo(20);
    }

    @Test
    public void homeworkExDiv02() {
        assertThat(c.compute(15, 5, "/")).isNotEqualTo(5);
    }

    @Test
    public void homeworkExSqrt01() {
        assertThat(c.compute(121, 0, "SQRT")).isEqualTo(11);
    }

    @Test
    public void homeworkExSqrt02() {
        assertThat(c.compute(121, 0, "SQRT")).isNotEqualTo(10);
    }

}
