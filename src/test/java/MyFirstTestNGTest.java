import Curs7.Calculator;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.awt.*;

@Test

public class MyFirstTestNGTest {
    Calculator c;

    @BeforeClass(groups = {"smoke"})
    public void setUp() {
        c = new Calculator();
    }

    @Test(description = "This is a first testNG test", priority = 1, groups = {"smoke"})
    public void myTestMethod1() {
        System.out.println("This is a test method 1");
    }

    public void myTestMethod2() {
        System.out.println("This is a test method 2");
    }

    private void myPrivateMethod() {
        System.out.println("This is NOT a test method");
    }

    @Test(description = "This is test method to show priority")
    public void myTestMethod3() {
        System.out.println("This is a test method 3");
        this.myPrivateMethod();
    }

    @Test(groups = {"smoke"})
    public void testSum03() {
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test(dependsOnMethods = {"testSum03"}, groups = {"smoke"})
    public void testDependsOnMethod() {
        System.out.println(" ---->Depends on method run");
    }

    @Test(dependsOnMethods = {"testSum03"}, alwaysRun = true, groups = {"smoke", "regression"})
    public void testDependsOnMethodAlwaysRun() {
        System.out.println(" ---->Always Run depends on method run");
    }

    @Test(expectedExceptions = IllegalArgumentException.class, groups = {"smoke", "regression"})
    public void testException() {
        Assert.assertEquals(1000, c.compute(1000, 0, "test"), 0);
    }

    @Test(groups = {"smoke"})
    @Parameters({"email", "password"})
    public void testLogin(String email, String password) {
        System.out.println("Login with email: " + email + " and password: " + password);
    }

    @Test
    public void getParamsFromCMD() {
        String browser = System.getProperty("browser");
        System.out.println("Run this tests with browser: " + browser);
    }

    @DataProvider(name = "registrationData")
    public Object[][] registerDataProvider() {
        return new Object[][]{
                {"test@test.com", "Ion", "Vasile", "str. Ion Creanga"},
                {"maria@test.com", "Maria", "Popescu", "str. Turda"},
                {"vasilica@test.com", "Vasilica", "Ionescu", "str. Ciupercii din Picior"}
        };
    }

    @Test(dataProvider = "registrationData")
    public void registrationTest(String email, String prenume, String nume, String adresa) {
        System.out.println("Register with email: " + email + "\nNume & Prenume: " + nume + " " + prenume + "\nadresa: " + adresa);
    }

    @DataProvider(name = "CalculatorTest")
    public Object[][] CalculatorTest() {
        return new Object[][]{
                {2.0, 3.0, "+", 5.0},
                {10.0, 10.0, "*", 100.0},
                {9.0, 3.0, "/", 3.0}
        };
    }

    @Test(dataProvider = "CalculatorTest")
    public void testCalculatorwithDP(Double d1, Double d2, String operator, Double result) {
        Assert.assertEquals(result, c.compute(d1, d2, operator), 0);
    }
}
