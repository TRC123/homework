import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class registrationHomework {

    /** Salut, dintr0un motiv necunoscut, nu vrea sa-mi populeze fieldurile cu valorile setate de mine in data provider */

    WebDriver driver;
    String errorMsg1Text = "Invalid input. Please enter between 2 and 35 letters"; // first name + last name
    String errorMsg2Text = "Invalid input. Please enter a valid email address"; // email
    String errorMsg3Text = "Invalid input. Please enter between 4 and 35 letters or numbers"; // username
    String errorMsg4Text = "Invalid input. Please use a minimum of 8 characters"; // password
    String errorMsg5Text = "Please confirm your password"; //confirm passowrd

    @DataProvider(name = "registrationDP")
    public Iterator<Object[]> registrationDP() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        /** firstName, lastName, email, userName, pw, confirm pw, firstNameErrMsg, lastNameErrMsg, emailErrMsg, userNameErrMsg, pwErrMsg, confirmpwErrMsg*/
//        dp.add(new String[]{"", "", "", "", "", "", errorMsg1Text, errorMsg1Text, errorMsg2Text, errorMsg3Text, errorMsg4Text, errorMsg5Text});
        dp.add(new String[]{"testusername", "", "", "", "", "", "", errorMsg1Text, errorMsg2Text, errorMsg3Text, errorMsg4Text, errorMsg5Text});
//        dp.add(new String[]{"", "testpassword", "", "", "", "", errorMsg1Text, "", errorMsg2Text, errorMsg3Text, errorMsg4Text, errorMsg5Text});
//        dp.add(new String[]{"", "", "test@email.com", "", "", "", errorMsg1Text, errorMsg1Text, "", errorMsg3Text, errorMsg4Text, errorMsg5Text});
//        dp.add(new String[]{"", "", "", "testusername", "", "", errorMsg1Text, errorMsg1Text, errorMsg2Text, "", errorMsg4Text, errorMsg5Text});
//        dp.add(new String[]{"", "", "", "", "testpassword", "", errorMsg1Text, errorMsg1Text, errorMsg2Text, errorMsg3Text, "", errorMsg5Text});
//        dp.add(new String[]{"", "", "", "", "", "testpassword", errorMsg1Text, errorMsg1Text, errorMsg2Text, errorMsg3Text, errorMsg4Text, ""});

        return dp.iterator();
    }

    @Test(dataProvider = "registrationDP")
    public void registrationTest(String firstName, String lastName, String email, String userName, String pw, String confirmPw, String firstNameErrMsg, String lastNameErrMsg, String emailErrMsg, String userNameErrMsg, String pwErrMsg, String confirmpwErrMsg) {
        WebElement registerAnAccount = driver.findElement(By.id("register-tab"));
        WebElement firstNameInput = driver.findElement(By.id("inputFirstName"));
        WebElement lastNameInput = driver.findElement(By.id("inputLastName"));
        WebElement emailInput = driver.findElement(By.id("inputEmail"));
        WebElement usernameInput = driver.findElement(By.id("inputUsername"));
        WebElement pwInput = driver.findElement(By.id("inputPassword"));
        WebElement confirmPwInput = driver.findElement(By.id("inputPassword2"));
        WebElement dateOfBirth = driver.findElement(By.className("form-control"));
        WebElement submitButton = driver.findElement(By.id("register-submit"));

        registerAnAccount.click();
        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(userName);
        pwInput.clear();
        pwInput.sendKeys(pw);
        confirmPwInput.clear();
        confirmPwInput.sendKeys(confirmPw);
        submitButton.submit();


        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]/text()")); //first name error msg
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]/text()")); //last name error msg
        WebElement err3 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]")); //email error msg
        WebElement err4 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]/text()")); //username error msg
        WebElement err5 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]/text()")); //pw error msg
        WebElement err6 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]")); //confirm pw error msg

        Assert.assertEquals(err1.getText(), firstNameErrMsg);
        Assert.assertEquals(err2.getText(), lastNameErrMsg);
        Assert.assertEquals(err3.getText(), emailErrMsg);
        Assert.assertEquals(err4.getText(), userNameErrMsg);
        Assert.assertEquals(err5.getText(), pwErrMsg);
        Assert.assertEquals(err6.getText(), confirmpwErrMsg);

    }

    @BeforeMethod
    public void setUp() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html");
    }

    @AfterMethod
    public void quit()
    {
        driver.quit();
    }

}
