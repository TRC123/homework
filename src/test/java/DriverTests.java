import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class DriverTests {

    WebDriver driver;

    @Test
    public void myFirstDriverTest() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        driver = new ChromeDriver();

        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);
        //searchInput.sendKeys(Keys.ENTER); //ori folosim optiunea asta ori punem in linia de mai sus, cu virgula

        //List<WebElement> titles = driver.findElements(By.xpath("//*[@id=\"rso\"]/div[*]/div/div[1]/a/h3/span"));
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        // \" \" = "" in xpath link


        //driver.quit(); //inchide tot browserul
        //driver.close(); //inchide tabul/fereastra curenta
    }

    @Test
    public void htmlUnitTest() {
        driver = new HtmlUnitDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    @Test
    public void firefoxDriverTest() {
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        //driver.quit();

    }

    @Test
    public void edgeDriverTest() {
        System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/msedgedriver.exe");
        driver = new EdgeDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        //driver.quit();

    }

    @Test
    public void ieDriverTest() {
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
        driver = new InternetExplorerDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    @Test //cea mai recomandata varianta este aceasta cu webdrivermanager
    public void driverManagerTest() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup(); /**this will be moved to @Before */
        driver = new ChromeDriver();//this will be moved to @Before
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("Numarul de rezultate este: " + titles.size());
        driver.quit(); //this will be moved to @After
    }

    @Test
    public void lazyButtonTest() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/lazy.html");
//        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//        for (int i = 0; i < 5; i++) {
//            WebElement lazybutton = driver.findElement(By.id("lazy-button"));
//            lazybutton.click();
//            //Thread.sleep(15000); /** !!! NU ESTE RECOMANDAT SA FOLOSIM THREAD.SLEEP */
//        }
        WebDriverWait wait = new WebDriverWait(driver, 15);
        for (int i = 0; i < 5; i++) {
            WebElement lazybutton = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("lazy-button"))); /** !!! modul recomandat pentru a face wait pe pagina */
            lazybutton.click();
        }
        driver.quit();
    }

    @DataProvider(name = "loginDP")
    public Iterator<Object[]> loginDP() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        //username, password, username error message, password error message, general error message
        dp.add(new String[] {"", "", "Please enter your username", "Please enter your password", ""});
        dp.add(new String[] {"aaa", "", "", "Please enter your password", ""});
        dp.add(new String[] {"", "aaaa", "Please enter your username", "", ""});
        dp.add(new String[] {"aaaa", "aaaa", "", "", "Invalid username or password!"});
        return dp.iterator();
    }

    @Test(dataProvider = "loginDP")
    public void negativeLoginTest(String username, String password, String userErrMsg, String pwErrMsg, String generalErrMsg) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html");
        WebElement usernameInput = driver.findElement(By.id("input-login-username"));
        WebElement passwrodInput = driver.findElement(By.id("input-login-password"));
        WebElement submitButton = driver.findElement(By.id("login-submit"));

        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwrodInput.clear();
        passwrodInput.sendKeys(password);
        submitButton.submit();


        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[2]/div/div[2]"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[3]/div/div[2]"));
        WebElement err3 = driver.findElement(By.id("login-error"));

//        System.out.println(err1.getText());
//        System.out.println(err2.getText());
//        System.out.println(err3.getText());
        Assert.assertEquals(err1.getText(), userErrMsg);
        Assert.assertEquals(err2.getText(), pwErrMsg);
        Assert.assertEquals(err3.getText(), generalErrMsg);

        driver.close();


    }

}
