function onSubmit(event) {
	event.preventDefault();
	
	var firstName = document.getElementById('firstName').value;
	if(firstName.length === 0) {
		
		return alert('First name field is empty');
	}
	if(firstName.length < 2 || firstName.length > 40) {
		return alert('First name field requires at least 2 characters and a maximum of 40.');
	}
	console.log('firstName', firstName);
	
	var lastName = document.getElementById('lastName').value;
	if(lastName.length === 0) {
		return alert('Last name field is empty');
	}
	if(lastName.length < 2 || lastName.length > 40) {
		return alert('Last name field requires at least 2 characters and a maximum of 40');
	}
	console.log('lastName', lastName);
	
	var email = document.getElementById('email').value;
	if(email.length === 0) {
		return alert('E-mail address field is empty');
	}
	console.log('email', email);
	
	var userName = document.getElementById('userName').value;
	if (userName.length === 0) {
		return alert('User name field is empty');
	}
	if(userName.length < 2 || userName.length > 40) {
		return alert('User name field requires at least 2 characters and a maximum of 40');
	}
	console.log('userName', userName);
	
	var password = document.getElementById('password').value;
	if (password.length === 0) {
		return alert('Password field is empty');
	}
	if(password.length < 2 || password.length > 40) {
		return alert('Password field requires at least 2 characters and a maximum of 40');
	}
	console.log('password', password);	
	
	var confirmPassword = document.getElementById('confirmPassword').value;
	if (confirmPassword.length === 0) {
		return alert('Confirm password field is empty');
	}
	if(confirmPassword.length < 2 || confirmPassword.length > 40) {
		return alert('Confirm password field requires at least 2 characters and a maximum of 40');
	}
	console.log('confirmPassword', confirmPassword);
	
	var aboutYourself = document.getElementById('aboutYourself').value;
	if (aboutYourself.lenght === 0) {
		return alert('About yourself field is empty');
	}
	if(aboutYourself.length < 2 || aboutYourself.length > 40) {
		return alert('About yourself field requires at least 2 characters and a maximum of 40');
	}
	console.log('aboutYourself', aboutYourself);
	
	var isGenderSelected = document.getElementById('gender1').checked || document.getElementById('gender2').checked;
	if (isGenderSelected === false) {
		return alert('Please select a gender');
	}
	console.log('isGenderSelected', 
		document.getElementById('gender1').checked ? document.getElementById('gender1').value :
		document.getElementById('gender2').checked ? document.getElementById('gender2').value :
	'Undefined gender');
	
	var subscribeNewsletter = document.getElementById('subscribeNewsletter').checked;
	if(subscribeNewsletter === false) {
		
		return alert('Please subscribe to the newsletter');
	}
	console.log('subscribeNewsletter', subscribeNewsletter);
	
	return true;
	
	
}